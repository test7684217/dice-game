import { useState } from "react";
import "./App.css";

function App() {
  let [result, setResult] = useState("");
  let [player1, setPlayer1] = useState(1);

  let [player2, setPlayer2] = useState(1);

  const handleDie = () =>{
    let player1_number = Math.floor(Math.random() * 6) + 1;
    let player2_number = Math.floor(Math.random() * 6) + 1;
    setPlayer1(player1_number);
    setPlayer2(player2_number);

    if (player1_number == player2_number) {
      setResult("It's a Draw");
    } else if (player1_number > player2_number) {
      setResult("Player 1 wins");
    } else if (player1_number < player2_number) {
      setResult("Player 2 wins");
    }
  }

  return (
    <div className="h-[100vh] w-[100vw] justify-center flex items-center">
      <div className="flex w-fit h-fit flex-col justify-center items-center bg-black p-12 rounded-[25px] gap-5 m-5">
        <h1 id="result" className="text-[#ff0000] font-bold text-[30px]">{result}</h1>
        <div className="flex gap-10 flex-wrap w-full h-full items-center justify-center">
          <div className="player-1 flex flex-col items-center gap-3">
            <h1 className="text-[#FF0000] font-bold text-[25px] ">Player 1</h1>
            <img
              src={`./${player1}.png`}
              alt="player-1 die"
              className="hover:rotate-[180deg] transition-all ease duration-500 w-[200px] h-[200px]"
            />
          </div>
          <div className="player-2 flex flex-col items-center gap-3">
            <h1 className="text-[#FF0000] font-bold text-[25px] ">Player 2</h1>
            <img
              src={`./${player2}.png`}
              alt="player-2 die"
              className="hover:rotate-[180deg] transition-all ease duration-500 w-[200px] h-[200px]"
            />
          </div>
        </div>

        <button onClick={handleDie} className="bg-[#FF0000] text-white pl-4 pr-4 pt-2 pb-2 rounded-lg text-[1.2rem]">Play</button>
      </div>
    </div>
  );
}

export default App;
